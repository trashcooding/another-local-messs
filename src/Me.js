import React from 'react';
import img from './images/me.jpg';

class Me extends React.Component {
      constructor(props){
        super(props);
        // this.press = this.press.bind(this);
        // this.state = {display: "none"};
      }
    //
    // press(){
    //   this.setState({display:(this.state.display === "none")?"flex":"none"});
    //   console.log("condition on:" + this.state.display);
    // }

      render() {
        return(
          <article id="home" className={[this.props.test,"panel","intro"].join(' ')}>
            <header>
              <h1>Jane Doe</h1>
              <p>Senior Astral Projectionist</p>
            </header>
            <a href="#work" className="jumplink pic">
              <span className="arrow icon solid fa-chevron-right"><span>See my work</span></span>
              <img src={img} alt="" />
            </a>
          </article>
        );
      }
    }
export default Me;
