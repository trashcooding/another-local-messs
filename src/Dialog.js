import React from 'react';
import pic01 from './images/pic01.jpg';
import pic02 from './images/pic02.jpg';
import pic03 from './images/pic03.jpg';
import pic04 from './images/pic04.jpg';
import pic05 from './images/pic05.jpg';
import pic06 from './images/pic06.jpg';
import pic07 from './images/pic07.jpg';
import pic08 from './images/pic08.jpg';
import pic09 from './images/pic09.jpg';
import pic10 from './images/pic10.jpg';
import pic11 from './images/pic11.jpg';
import pic12 from './images/pic12.jpg';

class Dialog extends React.Component {
      render() {
        return(
          <article id="work" className={["panel"].join(' ')}>
            <header>
              <h2>Work</h2>
            </header>
            <p>
              Phasellus enim sapien, blandit ullamcorper elementum eu, condimentum eu elit.
              Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
              luctus elit eget interdum.
            </p>
            <section>
              <div class="row">
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic01} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic02} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic03} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic04} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic05} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic06} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic07} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic08} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic09} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic10} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic11} alt=""></img></a>
                </div>
                <div class="col-4 col-6-medium col-12-small">
                  <a href="#" class="image fit"><img src={pic12} alt=""></img></a>
                </div>
              </div>
            </section>
          </article>
        );
      }
    }
export default Dialog;
