import React from 'react';
import './css/index.css';
import Me from './Me';
import Dialog from './Dialog';

class Nav extends React.Component {
      constructor(props){
        super(props);
        this.press = this.press.bind(this);
        this.state = {display: "none"};
      }

      press(){
        this.setState({display:(this.state.display === "none")?"flex":"none"});
        console.log("flag on:" + this.state.display);
      }

      render() {
        return(
            <div id="main">
  					<nav id="nav">
  						<a href="#" className="icon-home solid fa-home" onClick={this.press}><span>Home</span></a>
  						<a href="#work" className="icon solid fa-folder"><span>Work</span></a>
  						<a href="#contact" className="icon solid fa-envelope"><span>Contact</span></a>
  						<a href="https://vk.com/dan_kshn" className="icon brands fa-twitter"><span>Twitter</span></a>
  					</nav>
            <Me test={this.state.display}/>
            // <Dialog test={this.state.flag}/>
            </div>
        );
      }
    }
export default Nav;
