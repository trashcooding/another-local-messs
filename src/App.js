import React from 'react';
import Nav from './Nav';
import Footer from './Footer';
import './css/main.css';
import './css/noscript.css';
import './css/index.css';
function App() {
  return (
    <div id="wrapper">
    <Nav/>
    <Footer/>
    </div>
  );
}

export default App;
